package com.service.test0518.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2018-05-18T06:22:04.175Z")

@RestSchema(schemaId = "test0518")
@RequestMapping(path = "/test0518", produces = MediaType.APPLICATION_JSON)
public class Test0518Impl {

    @Autowired
    private Test0518Delegate userTest0518Delegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userTest0518Delegate.helloworld(name);
    }

}
