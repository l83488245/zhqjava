package com.service.test0518.controller;

import org.springframework.stereotype.Component;


@Component
public class Test0518Delegate {

    public String helloworld(String name){

        // Do Some Magic Here!
        return name;
    }
}
